import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { DataminerFeedDataMinerModule } from './feed/feed-data-miner.module';
import { DataminerCommentDataMinerModule } from './comment/comment-data-miner.module';
import { DataminerReactionDataMinerModule } from './reaction/reaction-data-miner.module';
import { DataminerKeywordDataMinerModule } from './keyword/keyword-data-miner.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        DataminerFeedDataMinerModule,
        DataminerCommentDataMinerModule,
        DataminerReactionDataMinerModule,
        DataminerKeywordDataMinerModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DataminerEntityModule {}
