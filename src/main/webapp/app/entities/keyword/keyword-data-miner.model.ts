import { BaseEntity } from './../../shared';

export class KeywordDataMiner implements BaseEntity {
    constructor(
        public id?: string,
        public keyword?: string,
    ) {
    }
}
