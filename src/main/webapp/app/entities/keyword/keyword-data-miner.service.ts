import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { KeywordDataMiner } from './keyword-data-miner.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class KeywordDataMinerService {

    private resourceUrl = 'api/keywords';

    constructor(private http: Http) { }

    create(keyword: KeywordDataMiner): Observable<KeywordDataMiner> {
        const copy = this.convert(keyword);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(keyword: KeywordDataMiner): Observable<KeywordDataMiner> {
        const copy = this.convert(keyword);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: string): Observable<KeywordDataMiner> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: string): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(keyword: KeywordDataMiner): KeywordDataMiner {
        const copy: KeywordDataMiner = Object.assign({}, keyword);
        return copy;
    }
}
