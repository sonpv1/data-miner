import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { KeywordDataMiner } from './keyword-data-miner.model';
import { KeywordDataMinerPopupService } from './keyword-data-miner-popup.service';
import { KeywordDataMinerService } from './keyword-data-miner.service';

@Component({
    selector: 'jhi-keyword-data-miner-dialog',
    templateUrl: './keyword-data-miner-dialog.component.html'
})
export class KeywordDataMinerDialogComponent implements OnInit {

    keyword: KeywordDataMiner;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private keywordService: KeywordDataMinerService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.keyword.id !== undefined) {
            this.subscribeToSaveResponse(
                this.keywordService.update(this.keyword));
        } else {
            this.subscribeToSaveResponse(
                this.keywordService.create(this.keyword));
        }
    }

    private subscribeToSaveResponse(result: Observable<KeywordDataMiner>) {
        result.subscribe((res: KeywordDataMiner) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: KeywordDataMiner) {
        this.eventManager.broadcast({ name: 'keywordListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-keyword-data-miner-popup',
    template: ''
})
export class KeywordDataMinerPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private keywordPopupService: KeywordDataMinerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.keywordPopupService
                    .open(KeywordDataMinerDialogComponent as Component, params['id']);
            } else {
                this.keywordPopupService
                    .open(KeywordDataMinerDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
