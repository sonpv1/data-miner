import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { KeywordDataMiner } from './keyword-data-miner.model';
import { KeywordDataMinerPopupService } from './keyword-data-miner-popup.service';
import { KeywordDataMinerService } from './keyword-data-miner.service';

@Component({
    selector: 'jhi-keyword-data-miner-delete-dialog',
    templateUrl: './keyword-data-miner-delete-dialog.component.html'
})
export class KeywordDataMinerDeleteDialogComponent {

    keyword: KeywordDataMiner;

    constructor(
        private keywordService: KeywordDataMinerService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.keywordService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'keywordListModification',
                content: 'Deleted an keyword'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-keyword-data-miner-delete-popup',
    template: ''
})
export class KeywordDataMinerDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private keywordPopupService: KeywordDataMinerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.keywordPopupService
                .open(KeywordDataMinerDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
