import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { KeywordDataMiner } from './keyword-data-miner.model';
import { KeywordDataMinerService } from './keyword-data-miner.service';

@Component({
    selector: 'jhi-keyword-data-miner-detail',
    templateUrl: './keyword-data-miner-detail.component.html'
})
export class KeywordDataMinerDetailComponent implements OnInit, OnDestroy {

    keyword: KeywordDataMiner;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private keywordService: KeywordDataMinerService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInKeywords();
    }

    load(id) {
        this.keywordService.find(id).subscribe((keyword) => {
            this.keyword = keyword;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInKeywords() {
        this.eventSubscriber = this.eventManager.subscribe(
            'keywordListModification',
            (response) => this.load(this.keyword.id)
        );
    }
}
