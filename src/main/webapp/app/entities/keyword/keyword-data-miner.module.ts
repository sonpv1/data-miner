import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DataminerSharedModule } from '../../shared';
import {
    KeywordDataMinerService,
    KeywordDataMinerPopupService,
    KeywordDataMinerComponent,
    KeywordDataMinerDetailComponent,
    KeywordDataMinerDialogComponent,
    KeywordDataMinerPopupComponent,
    KeywordDataMinerDeletePopupComponent,
    KeywordDataMinerDeleteDialogComponent,
    keywordRoute,
    keywordPopupRoute,
    KeywordDataMinerResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...keywordRoute,
    ...keywordPopupRoute,
];

@NgModule({
    imports: [
        DataminerSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        KeywordDataMinerComponent,
        KeywordDataMinerDetailComponent,
        KeywordDataMinerDialogComponent,
        KeywordDataMinerDeleteDialogComponent,
        KeywordDataMinerPopupComponent,
        KeywordDataMinerDeletePopupComponent,
    ],
    entryComponents: [
        KeywordDataMinerComponent,
        KeywordDataMinerDialogComponent,
        KeywordDataMinerPopupComponent,
        KeywordDataMinerDeleteDialogComponent,
        KeywordDataMinerDeletePopupComponent,
    ],
    providers: [
        KeywordDataMinerService,
        KeywordDataMinerPopupService,
        KeywordDataMinerResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DataminerKeywordDataMinerModule {}
