import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { KeywordDataMinerComponent } from './keyword-data-miner.component';
import { KeywordDataMinerDetailComponent } from './keyword-data-miner-detail.component';
import { KeywordDataMinerPopupComponent } from './keyword-data-miner-dialog.component';
import { KeywordDataMinerDeletePopupComponent } from './keyword-data-miner-delete-dialog.component';

@Injectable()
export class KeywordDataMinerResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const keywordRoute: Routes = [
    {
        path: 'keyword-data-miner',
        component: KeywordDataMinerComponent,
        resolve: {
            'pagingParams': KeywordDataMinerResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dataminerApp.keyword.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'keyword-data-miner/:id',
        component: KeywordDataMinerDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dataminerApp.keyword.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const keywordPopupRoute: Routes = [
    {
        path: 'keyword-data-miner-new',
        component: KeywordDataMinerPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dataminerApp.keyword.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'keyword-data-miner/:id/edit',
        component: KeywordDataMinerPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dataminerApp.keyword.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'keyword-data-miner/:id/delete',
        component: KeywordDataMinerDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dataminerApp.keyword.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
