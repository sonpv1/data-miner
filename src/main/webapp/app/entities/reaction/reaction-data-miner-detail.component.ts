import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { ReactionDataMiner } from './reaction-data-miner.model';
import { ReactionDataMinerService } from './reaction-data-miner.service';

@Component({
    selector: 'jhi-reaction-data-miner-detail',
    templateUrl: './reaction-data-miner-detail.component.html'
})
export class ReactionDataMinerDetailComponent implements OnInit, OnDestroy {

    reaction: ReactionDataMiner;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private reactionService: ReactionDataMinerService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInReactions();
    }

    load(id) {
        this.reactionService.find(id).subscribe((reaction) => {
            this.reaction = reaction;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInReactions() {
        this.eventSubscriber = this.eventManager.subscribe(
            'reactionListModification',
            (response) => this.load(this.reaction.id)
        );
    }
}
