import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ReactionDataMiner } from './reaction-data-miner.model';
import { ReactionDataMinerPopupService } from './reaction-data-miner-popup.service';
import { ReactionDataMinerService } from './reaction-data-miner.service';

@Component({
    selector: 'jhi-reaction-data-miner-dialog',
    templateUrl: './reaction-data-miner-dialog.component.html'
})
export class ReactionDataMinerDialogComponent implements OnInit {

    reaction: ReactionDataMiner;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private reactionService: ReactionDataMinerService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.reaction.id !== undefined) {
            this.subscribeToSaveResponse(
                this.reactionService.update(this.reaction));
        } else {
            this.subscribeToSaveResponse(
                this.reactionService.create(this.reaction));
        }
    }

    private subscribeToSaveResponse(result: Observable<ReactionDataMiner>) {
        result.subscribe((res: ReactionDataMiner) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: ReactionDataMiner) {
        this.eventManager.broadcast({ name: 'reactionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-reaction-data-miner-popup',
    template: ''
})
export class ReactionDataMinerPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private reactionPopupService: ReactionDataMinerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.reactionPopupService
                    .open(ReactionDataMinerDialogComponent as Component, params['id']);
            } else {
                this.reactionPopupService
                    .open(ReactionDataMinerDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
