import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DataminerSharedModule } from '../../shared';
import {
    ReactionDataMinerService,
    ReactionDataMinerPopupService,
    ReactionDataMinerComponent,
    ReactionDataMinerDetailComponent,
    ReactionDataMinerDialogComponent,
    ReactionDataMinerPopupComponent,
    ReactionDataMinerDeletePopupComponent,
    ReactionDataMinerDeleteDialogComponent,
    reactionRoute,
    reactionPopupRoute,
    ReactionDataMinerResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...reactionRoute,
    ...reactionPopupRoute,
];

@NgModule({
    imports: [
        DataminerSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        ReactionDataMinerComponent,
        ReactionDataMinerDetailComponent,
        ReactionDataMinerDialogComponent,
        ReactionDataMinerDeleteDialogComponent,
        ReactionDataMinerPopupComponent,
        ReactionDataMinerDeletePopupComponent,
    ],
    entryComponents: [
        ReactionDataMinerComponent,
        ReactionDataMinerDialogComponent,
        ReactionDataMinerPopupComponent,
        ReactionDataMinerDeleteDialogComponent,
        ReactionDataMinerDeletePopupComponent,
    ],
    providers: [
        ReactionDataMinerService,
        ReactionDataMinerPopupService,
        ReactionDataMinerResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DataminerReactionDataMinerModule {}
