import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ReactionDataMiner } from './reaction-data-miner.model';
import { ReactionDataMinerPopupService } from './reaction-data-miner-popup.service';
import { ReactionDataMinerService } from './reaction-data-miner.service';

@Component({
    selector: 'jhi-reaction-data-miner-delete-dialog',
    templateUrl: './reaction-data-miner-delete-dialog.component.html'
})
export class ReactionDataMinerDeleteDialogComponent {

    reaction: ReactionDataMiner;

    constructor(
        private reactionService: ReactionDataMinerService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.reactionService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'reactionListModification',
                content: 'Deleted an reaction'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-reaction-data-miner-delete-popup',
    template: ''
})
export class ReactionDataMinerDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private reactionPopupService: ReactionDataMinerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.reactionPopupService
                .open(ReactionDataMinerDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
