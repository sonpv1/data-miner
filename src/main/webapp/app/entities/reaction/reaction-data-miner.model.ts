import { BaseEntity } from './../../shared';

export class ReactionDataMiner implements BaseEntity {
    constructor(
        public id?: string,
        public name?: string,
        public type?: string,
    ) {
    }
}
