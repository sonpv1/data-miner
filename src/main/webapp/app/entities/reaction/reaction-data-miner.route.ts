import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ReactionDataMinerComponent } from './reaction-data-miner.component';
import { ReactionDataMinerDetailComponent } from './reaction-data-miner-detail.component';
import { ReactionDataMinerPopupComponent } from './reaction-data-miner-dialog.component';
import { ReactionDataMinerDeletePopupComponent } from './reaction-data-miner-delete-dialog.component';

@Injectable()
export class ReactionDataMinerResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const reactionRoute: Routes = [
    {
        path: 'reaction-data-miner',
        component: ReactionDataMinerComponent,
        resolve: {
            'pagingParams': ReactionDataMinerResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dataminerApp.reaction.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'reaction-data-miner/:id',
        component: ReactionDataMinerDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dataminerApp.reaction.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const reactionPopupRoute: Routes = [
    {
        path: 'reaction-data-miner-new',
        component: ReactionDataMinerPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dataminerApp.reaction.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'reaction-data-miner/:id/edit',
        component: ReactionDataMinerPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dataminerApp.reaction.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'reaction-data-miner/:id/delete',
        component: ReactionDataMinerDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dataminerApp.reaction.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
