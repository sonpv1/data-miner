import { BaseEntity } from './../../shared';

export class FeedDataMiner implements BaseEntity {
    constructor(
        public id?: string,
        public fbid?: string,
        public title?: string,
        public url?: string,
        public imageUrl?: string,
        public fbpostType?: string,
        public likes?: number,
        public comments?: number,
        public shares?: string,
        public fbpostDate?: string,
        public description?: string,
        public link?: string,
        public fbpage?: string,
        public lastUpdate?: number,
    ) {
    }
}
