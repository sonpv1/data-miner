import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { FeedDataMinerComponent } from './feed-data-miner.component';
import { FeedDataMinerDetailComponent } from './feed-data-miner-detail.component';
import { FeedDataMinerPopupComponent } from './feed-data-miner-dialog.component';
import { FeedDataMinerDeletePopupComponent } from './feed-data-miner-delete-dialog.component';

export const feedRoute: Routes = [
    {
        path: 'feed-data-miner',
        component: FeedDataMinerComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dataminerApp.feed.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'feed-data-miner/:id',
        component: FeedDataMinerDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dataminerApp.feed.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const feedPopupRoute: Routes = [
    {
        path: 'feed-data-miner-new',
        component: FeedDataMinerPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dataminerApp.feed.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'feed-data-miner/:id/edit',
        component: FeedDataMinerPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dataminerApp.feed.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'feed-data-miner/:id/delete',
        component: FeedDataMinerDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dataminerApp.feed.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
