import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { FeedDataMiner } from './feed-data-miner.model';
import { FeedDataMinerPopupService } from './feed-data-miner-popup.service';
import { FeedDataMinerService } from './feed-data-miner.service';

@Component({
    selector: 'jhi-feed-data-miner-dialog',
    templateUrl: './feed-data-miner-dialog.component.html'
})
export class FeedDataMinerDialogComponent implements OnInit {

    feed: FeedDataMiner;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private feedService: FeedDataMinerService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.feed.id !== undefined) {
            this.subscribeToSaveResponse(
                this.feedService.update(this.feed));
        } else {
            this.subscribeToSaveResponse(
                this.feedService.create(this.feed));
        }
    }

    private subscribeToSaveResponse(result: Observable<FeedDataMiner>) {
        result.subscribe((res: FeedDataMiner) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: FeedDataMiner) {
        this.eventManager.broadcast({ name: 'feedListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-feed-data-miner-popup',
    template: ''
})
export class FeedDataMinerPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private feedPopupService: FeedDataMinerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.feedPopupService
                    .open(FeedDataMinerDialogComponent as Component, params['id']);
            } else {
                this.feedPopupService
                    .open(FeedDataMinerDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
