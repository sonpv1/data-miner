import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DataminerSharedModule } from '../../shared';
import {
    FeedDataMinerService,
    FeedDataMinerPopupService,
    FeedDataMinerComponent,
    FeedDataMinerDetailComponent,
    FeedDataMinerDialogComponent,
    FeedDataMinerPopupComponent,
    FeedDataMinerDeletePopupComponent,
    FeedDataMinerDeleteDialogComponent,
    feedRoute,
    feedPopupRoute,
} from './';

const ENTITY_STATES = [
    ...feedRoute,
    ...feedPopupRoute,
];

@NgModule({
    imports: [
        DataminerSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        FeedDataMinerComponent,
        FeedDataMinerDetailComponent,
        FeedDataMinerDialogComponent,
        FeedDataMinerDeleteDialogComponent,
        FeedDataMinerPopupComponent,
        FeedDataMinerDeletePopupComponent,
    ],
    entryComponents: [
        FeedDataMinerComponent,
        FeedDataMinerDialogComponent,
        FeedDataMinerPopupComponent,
        FeedDataMinerDeleteDialogComponent,
        FeedDataMinerDeletePopupComponent,
    ],
    providers: [
        FeedDataMinerService,
        FeedDataMinerPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DataminerFeedDataMinerModule {}
