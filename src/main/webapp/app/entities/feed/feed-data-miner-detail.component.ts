import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { FeedDataMiner } from './feed-data-miner.model';
import { FeedDataMinerService } from './feed-data-miner.service';

@Component({
    selector: 'jhi-feed-data-miner-detail',
    templateUrl: './feed-data-miner-detail.component.html'
})
export class FeedDataMinerDetailComponent implements OnInit, OnDestroy {

    feed: FeedDataMiner;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private feedService: FeedDataMinerService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInFeeds();
    }

    load(id) {
        this.feedService.find(id).subscribe((feed) => {
            this.feed = feed;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInFeeds() {
        this.eventSubscriber = this.eventManager.subscribe(
            'feedListModification',
            (response) => this.load(this.feed.id)
        );
    }
}
