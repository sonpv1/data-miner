import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { FeedDataMiner } from './feed-data-miner.model';
import { FeedDataMinerPopupService } from './feed-data-miner-popup.service';
import { FeedDataMinerService } from './feed-data-miner.service';

@Component({
    selector: 'jhi-feed-data-miner-delete-dialog',
    templateUrl: './feed-data-miner-delete-dialog.component.html'
})
export class FeedDataMinerDeleteDialogComponent {

    feed: FeedDataMiner;

    constructor(
        private feedService: FeedDataMinerService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.feedService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'feedListModification',
                content: 'Deleted an feed'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-feed-data-miner-delete-popup',
    template: ''
})
export class FeedDataMinerDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private feedPopupService: FeedDataMinerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.feedPopupService
                .open(FeedDataMinerDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
