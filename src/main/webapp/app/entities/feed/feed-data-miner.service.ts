import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { FeedDataMiner } from './feed-data-miner.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class FeedDataMinerService {

    private resourceUrl = 'api/feeds';

    constructor(private http: Http) { }

    create(feed: FeedDataMiner): Observable<FeedDataMiner> {
        const copy = this.convert(feed);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(feed: FeedDataMiner): Observable<FeedDataMiner> {
        const copy = this.convert(feed);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: string): Observable<FeedDataMiner> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: string): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(feed: FeedDataMiner): FeedDataMiner {
        const copy: FeedDataMiner = Object.assign({}, feed);
        return copy;
    }
}
