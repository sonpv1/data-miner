import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DataminerSharedModule } from '../../shared';
import {
    CommentDataMinerService,
    CommentDataMinerPopupService,
    CommentDataMinerComponent,
    CommentDataMinerDetailComponent,
    CommentDataMinerDialogComponent,
    CommentDataMinerPopupComponent,
    CommentDataMinerDeletePopupComponent,
    CommentDataMinerDeleteDialogComponent,
    commentRoute,
    commentPopupRoute,
    CommentDataMinerResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...commentRoute,
    ...commentPopupRoute,
];

@NgModule({
    imports: [
        DataminerSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        CommentDataMinerComponent,
        CommentDataMinerDetailComponent,
        CommentDataMinerDialogComponent,
        CommentDataMinerDeleteDialogComponent,
        CommentDataMinerPopupComponent,
        CommentDataMinerDeletePopupComponent,
    ],
    entryComponents: [
        CommentDataMinerComponent,
        CommentDataMinerDialogComponent,
        CommentDataMinerPopupComponent,
        CommentDataMinerDeleteDialogComponent,
        CommentDataMinerDeletePopupComponent,
    ],
    providers: [
        CommentDataMinerService,
        CommentDataMinerPopupService,
        CommentDataMinerResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DataminerCommentDataMinerModule {}
