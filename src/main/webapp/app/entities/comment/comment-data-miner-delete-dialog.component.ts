import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CommentDataMiner } from './comment-data-miner.model';
import { CommentDataMinerPopupService } from './comment-data-miner-popup.service';
import { CommentDataMinerService } from './comment-data-miner.service';

@Component({
    selector: 'jhi-comment-data-miner-delete-dialog',
    templateUrl: './comment-data-miner-delete-dialog.component.html'
})
export class CommentDataMinerDeleteDialogComponent {

    comment: CommentDataMiner;

    constructor(
        private commentService: CommentDataMinerService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.commentService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'commentListModification',
                content: 'Deleted an comment'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-comment-data-miner-delete-popup',
    template: ''
})
export class CommentDataMinerDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private commentPopupService: CommentDataMinerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.commentPopupService
                .open(CommentDataMinerDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
