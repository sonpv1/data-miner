import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { CommentDataMiner } from './comment-data-miner.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class CommentDataMinerService {

    private resourceUrl = 'api/comments';

    constructor(private http: Http) { }

    create(comment: CommentDataMiner): Observable<CommentDataMiner> {
        const copy = this.convert(comment);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    update(comment: CommentDataMiner): Observable<CommentDataMiner> {
        const copy = this.convert(comment);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            return res.json();
        });
    }

    find(id: string): Observable<CommentDataMiner> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            return res.json();
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: string): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convert(comment: CommentDataMiner): CommentDataMiner {
        const copy: CommentDataMiner = Object.assign({}, comment);
        return copy;
    }
}
