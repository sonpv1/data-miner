import { BaseEntity } from './../../shared';

export class CommentDataMiner implements BaseEntity {
    constructor(
        public id?: string,
        public fbid?: string,
        public message?: string,
        public createdTime?: string,
    ) {
    }
}
