import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { CommentDataMinerComponent } from './comment-data-miner.component';
import { CommentDataMinerDetailComponent } from './comment-data-miner-detail.component';
import { CommentDataMinerPopupComponent } from './comment-data-miner-dialog.component';
import { CommentDataMinerDeletePopupComponent } from './comment-data-miner-delete-dialog.component';

@Injectable()
export class CommentDataMinerResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const commentRoute: Routes = [
    {
        path: 'comment-data-miner',
        component: CommentDataMinerComponent,
        resolve: {
            'pagingParams': CommentDataMinerResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dataminerApp.comment.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'comment-data-miner/:id',
        component: CommentDataMinerDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dataminerApp.comment.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const commentPopupRoute: Routes = [
    {
        path: 'comment-data-miner-new',
        component: CommentDataMinerPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dataminerApp.comment.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'comment-data-miner/:id/edit',
        component: CommentDataMinerPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dataminerApp.comment.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'comment-data-miner/:id/delete',
        component: CommentDataMinerDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'dataminerApp.comment.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
