package net.crash.service;

import net.crash.domain.Keyword;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Keyword.
 */
public interface KeywordService {

    /**
     * Save a keyword.
     *
     * @param keyword the entity to save
     * @return the persisted entity
     */
    Keyword save(Keyword keyword);

    /**
     *  Get all the keywords.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Keyword> findAll(Pageable pageable);

    /**
     *  Get the "id" keyword.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Keyword findOne(String id);

    /**
     *  Delete the "id" keyword.
     *
     *  @param id the id of the entity
     */
    void delete(String id);
}
