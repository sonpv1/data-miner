package net.crash.service;

import net.crash.domain.Feed;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Service Interface for managing Feed.
 */
public interface FeedService {

    /**
     * Save a list of feed.
     *
     * @param feeds the entity to save
     * @return the persisted entity
     */
    List<Feed> save(List<Feed> feeds);

    /**
     * Save a feed.
     *
     * @param feed the entity to save
     * @return the persisted entity
     */
    Feed save(Feed feed);

    /**
     * Get all the feeds.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Feed> findAll(Pageable pageable);

    /**
     * Get the "id" feed.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Feed findOne(String id);

    /**
     * Delete the "id" feed.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
