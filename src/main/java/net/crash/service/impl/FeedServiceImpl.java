package net.crash.service.impl;

import net.crash.service.FeedService;
import net.crash.domain.Feed;
import net.crash.repository.FeedRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Service Implementation for managing Feed.
 */
@Service
public class FeedServiceImpl implements FeedService{

    private final Logger log = LoggerFactory.getLogger(FeedServiceImpl.class);

    private final FeedRepository feedRepository;

    public FeedServiceImpl(FeedRepository feedRepository) {
        this.feedRepository = feedRepository;
    }

    @Override
    public List<Feed> save(List<Feed> feeds) {
        log.debug("Request to save List of Feeds : {}", feeds);
        return feedRepository.save(feeds);
    }

    /**
     * Save a feed.
     *
     * @param feed the entity to save
     * @return the persisted entity
     */
    @Override
    public Feed save(Feed feed) {
        log.debug("Request to save Feed : {}", feed);
        return feedRepository.save(feed);
    }

    /**
     *  Get all the feeds.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    public Page<Feed> findAll(Pageable pageable) {
        log.debug("Request to get all Feeds");
        return feedRepository.findAll(pageable);
    }

    /**
     *  Get one feed by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    public Feed findOne(String id) {
        log.debug("Request to get Feed : {}", id);
        return feedRepository.findOne(id);
    }

    /**
     *  Delete the  feed by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Feed : {}", id);
        feedRepository.delete(id);
    }
}
