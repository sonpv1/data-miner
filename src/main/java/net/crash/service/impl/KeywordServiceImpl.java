package net.crash.service.impl;

import net.crash.service.KeywordService;
import net.crash.domain.Keyword;
import net.crash.repository.KeywordRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


/**
 * Service Implementation for managing Keyword.
 */
@Service
public class KeywordServiceImpl implements KeywordService{

    private final Logger log = LoggerFactory.getLogger(KeywordServiceImpl.class);

    private final KeywordRepository keywordRepository;

    public KeywordServiceImpl(KeywordRepository keywordRepository) {
        this.keywordRepository = keywordRepository;
    }

    /**
     * Save a keyword.
     *
     * @param keyword the entity to save
     * @return the persisted entity
     */
    @Override
    public Keyword save(Keyword keyword) {
        log.debug("Request to save Keyword : {}", keyword);
        return keywordRepository.save(keyword);
    }

    /**
     *  Get all the keywords.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    public Page<Keyword> findAll(Pageable pageable) {
        log.debug("Request to get all Keywords");
        return keywordRepository.findAll(pageable);
    }

    /**
     *  Get one keyword by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    public Keyword findOne(String id) {
        log.debug("Request to get Keyword : {}", id);
        return keywordRepository.findOne(id);
    }

    /**
     *  Delete the  keyword by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Keyword : {}", id);
        keywordRepository.delete(id);
    }
}
