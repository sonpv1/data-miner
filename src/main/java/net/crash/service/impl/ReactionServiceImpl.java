package net.crash.service.impl;

import net.crash.service.ReactionService;
import net.crash.domain.Reaction;
import net.crash.repository.ReactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Service Implementation for managing Reaction.
 */
@Service
public class ReactionServiceImpl implements ReactionService{

    private final Logger log = LoggerFactory.getLogger(ReactionServiceImpl.class);

    private final ReactionRepository reactionRepository;

    public ReactionServiceImpl(ReactionRepository reactionRepository) {
        this.reactionRepository = reactionRepository;
    }

    @Override
    public List<Reaction> save(List<Reaction> reactions) {
        log.debug("Request to save List of Reactions : {}", reactions);
        return reactionRepository.save(reactions);
    }

    /**
     * Save a reaction.
     *
     * @param reaction the entity to save
     * @return the persisted entity
     */
    @Override
    public Reaction save(Reaction reaction) {
        log.debug("Request to save Reaction : {}", reaction);
        return reactionRepository.save(reaction);
    }

    /**
     *  Get all the reactions.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    public Page<Reaction> findAll(Pageable pageable) {
        log.debug("Request to get all Reactions");
        return reactionRepository.findAll(pageable);
    }

    /**
     *  Get one reaction by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    public Reaction findOne(String id) {
        log.debug("Request to get Reaction : {}", id);
        return reactionRepository.findOne(id);
    }

    /**
     *  Delete the  reaction by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Reaction : {}", id);
        reactionRepository.delete(id);
    }
}
