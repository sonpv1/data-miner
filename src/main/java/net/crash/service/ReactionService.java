package net.crash.service;

import net.crash.domain.Reaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Service Interface for managing Reaction.
 */
public interface ReactionService {

    /**
     * Save a list of reaction.
     *
     * @param reactions the entity to save
     * @return the persisted entity
     */
    List<Reaction> save(List<Reaction> reactions);


    /**
     * Save a reaction.
     *
     * @param reaction the entity to save
     * @return the persisted entity
     */
    Reaction save(Reaction reaction);

    /**
     *  Get all the reactions.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Reaction> findAll(Pageable pageable);

    /**
     *  Get the "id" reaction.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Reaction findOne(String id);

    /**
     *  Delete the "id" reaction.
     *
     *  @param id the id of the entity
     */
    void delete(String id);
}
