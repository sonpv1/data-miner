package net.crash.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**
 * Created by Pi on 7/21/2017.
 */
public class IOUtils {

    private final static Logger LOGGER = LoggerFactory.getLogger(IOUtils.class);

    /**
     * To save object to file
     * @param obj
     * @param fileName
     */
    public static void save(Object obj, String fileName) {
        try (FileOutputStream fos = new FileOutputStream(fileName);
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(obj);
            org.apache.commons.io.IOUtils.closeQuietly(oos);
            org.apache.commons.io.IOUtils.closeQuietly(fos);
        } catch (FileNotFoundException e) {
            LOGGER.error("SAVE FILE: " + fileName, e);
        } catch (IOException e) {
            LOGGER.error("SAVE FILE: " + fileName, e);
        }
    }

    /**
     * To read object from File
     * @param fileName
     * @return
     */
    public static Object load(String fileName) {
        Object obj = null;
        try (FileInputStream fis = new FileInputStream(fileName);
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            obj = ois.readObject();
            org.apache.commons.io.IOUtils.closeQuietly(ois);
            org.apache.commons.io.IOUtils.closeQuietly(fis);
        } catch (Exception ex) {
            LOGGER.error("OPEN FILE: " + fileName, ex);
        }

        return obj;
    }
}
