package net.crash.utils;

import org.apache.commons.lang3.StringEscapeUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Created by Pi on 7/18/2017.
 */
public class UrlBuilder {

    public static String getFacebookCommentFeedUrl(String baseUrl) {
        String fields = "&fields=id,message,reactions.limit(0).summary(true),created_time,comments,from";

        return baseUrl + fields;
    }

    /**
     * To decode url
     * @param url
     * @return
     */
    public static String decodeUrl(String url) {
        try {
            return URLDecoder.decode(StringEscapeUtils.unescapeJava(url), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return url;
        }
    }

    public static List<String> getReactionsForComments(String baseUrl) {
        List<String> urls = new ArrayList<>();
        String[] reactionTypes = new String[]{"LIKE", "LOVE", "WOW", "HAHA", "SAD", "ANGRY"};
        Stream.of(reactionTypes).forEach(item -> {
            String u = String.format("%s&fields=reactions.type(%s).limit(0).summary(total_count)", baseUrl, item);
            System.out.println(u);
            urls.add(u);
        });

        return urls;
    }

    /**
     * To build url
     *
     * @param host
     * @param map
     * @return
     */
    public static String build(String host, Map<String, String> map) {
        StringBuilder builder = new StringBuilder(host);
        map.forEach((x, y) -> {
            builder.append(x).append("=").append(encode(y)).append("&");
        });

        return builder.toString();
    }

    /**
     * To encode value
     *
     * @param value
     * @return
     */
    public static String encode(String value) {
        try {
            return URLEncoder.encode(value, "UTF-8").replace("+", "%20");
        } catch (UnsupportedEncodingException e) {
            return value;
        }
    }
}
