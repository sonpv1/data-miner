package net.crash.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Pi on 7/21/2017.
 */
public class DriverUtils {
    private static WebDriver webDriver;
    static {
        System.setProperty("webdriver.chrome.driver", DriverUtils.class.getResource("/webdrivers/chromedriver.exe").getFile());
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("profile.default_content_setting_values.notifications", 2);

        ChromeOptions options = new ChromeOptions();
        options.addArguments("test-type");
        options.addArguments("--disable-extensions");
        options.setExperimentalOption("prefs", prefs);

        DesiredCapabilities dc = DesiredCapabilities.chrome();
        dc.setCapability(ChromeOptions.CAPABILITY, options);
        webDriver = new ChromeDriver(dc);
    }

    /**
     * To get driver
     * @return
     */
    public static WebDriver getDriver() {
        return webDriver;
    }

}
