package net.crash.utils;


import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

/**
 * Created by Pi on 7/18/2017.
 */
public class HttpRequestUtils {

    /**
     * To send get request
     *
     * @param url
     * @return
     */
    public static String sendGet(String url) {
        try {
            CloseableHttpAsyncClient    client = HttpAsyncClients.custom().setUserAgent("Mozilla/5.0 Firefox/26.0").build();
            client.start();
            HttpGet              httpGet = new HttpGet(url);
            Future<HttpResponse> future  = client.execute(httpGet, null);
            int                  status  = future.get().getStatusLine().getStatusCode();
            client.close();

            if (200 == status) {
                return EntityUtils.toString(future.get().getEntity());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * To send post
     * @param url
     * @param p
     * @return
     */
    public static String sendPost(String url, Map<String, String> p) {
        try {
            CloseableHttpAsyncClient    client = HttpAsyncClients.custom().setUserAgent("Mozilla/5.0 Firefox/26.0").build();
            client.start();
            HttpPost            httpPost = new HttpPost(url);
            List<NameValuePair> params   = new ArrayList<>();
            p.forEach((x, y) -> params.add(new BasicNameValuePair(x, y)));
            httpPost.setEntity(new UrlEncodedFormEntity(params));

            Future<HttpResponse> future = client.execute(httpPost, null);
            int                  status = future.get().getStatusLine().getStatusCode();
            client.close();

            if (200 == status) {
                return EntityUtils.toString(future.get().getEntity());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
