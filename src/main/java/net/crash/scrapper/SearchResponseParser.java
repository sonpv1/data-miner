package net.crash.scrapper;

import net.crash.domain.SearchResponse;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Pi on 7/21/2017.
 */
@Component
public class SearchResponseParser {

    private final static String USER_HOVERCARD    = "/ajax/hovercard/user.php?id=";
    private final static String PAGE_HOVERCARD    = "/ajax/hovercard/page.php?id=";
    private final static String GROUP_HOVERCARD   = "/ajax/hovercard/group.php?id=";
    private static final String FACEBOOK_BASE_URL = "https://www.facebook.com/";
    private final static Logger LOGGER            = LoggerFactory.getLogger(SearchResponseParser.class);


    /**
     * To parse SearchResponse
     *
     * @param element
     * @return
     */
    public SearchResponse parse(WebElement element) {
        String   html         = element.getAttribute("innerHTML");
        Document document     = Jsoup.parse(html);
        Element  startElement = getStartElement(document.body().child(0));
        if (null == startElement) {
            LOGGER.info(html);
            return null;
        }
        SearchResponse searchResponse = new SearchResponse();
        Element        contentElement = startElement.child(1);
        Element        photoElement   = contentElement.getElementsByAttributeValue("rel", "theater").first();
        if (null == photoElement) return null;
        String photoUrl = photoElement.attr("href");

        String id = cleanPhotoPrefix(photoUrl);
        LOGGER.info("ID: " + id);
        LOGGER.info("PHOTO URL: " + photoUrl);
        searchResponse.setFbId(id);
        Element dtElement = contentElement.child(0);
        String  postUrl   = FACEBOOK_BASE_URL + dtElement.select("a").first().attr("href");
        LOGGER.info("POST URL: " + postUrl);
        searchResponse.setPostUrl(postUrl);
        Element  headerElement = startElement.child(0);
        Elements profileLinks  = headerElement.getElementsByAttribute("data-hovercard");
        for (Element profileLink : profileLinks) {
            String dataHovercard = profileLink.attr("data-hovercard");
            if (StringUtils.isNotEmpty(dataHovercard)) {
                if (dataHovercard.startsWith(USER_HOVERCARD)) {
                    LOGGER.info("USER: " + profileLink.text() + " ID -> " + cleanValue(dataHovercard));
                    searchResponse.setUserId(cleanValue(dataHovercard));
                    searchResponse.setUser(profileLink.text());
                }
                if (dataHovercard.startsWith(PAGE_HOVERCARD)) {
                    LOGGER.info("PAGE: " + profileLink.text() + " ID -> " + StringUtils.remove(dataHovercard, PAGE_HOVERCARD));
                    searchResponse.setPageId(cleanValue(dataHovercard));
                    searchResponse.setPage(profileLink.text());
                }
                if (dataHovercard.startsWith(GROUP_HOVERCARD)) {
                    LOGGER.info("GROUP: " + profileLink.text() + " ID -> " + cleanValue(dataHovercard));
                    searchResponse.setGroupId(cleanValue(dataHovercard));
                    searchResponse.setGroup(profileLink.text());
                }
            }
        }

        return searchResponse;
    }

    /**
     * To find start element
     *
     * @param element
     * @return
     */
    static Element getStartElement(Element element) {
        if (element.children().size() == 0) return null;
        if (element.attributes().size() > 0) {
            return getStartElement(element.child(0));
        }
        return element.child(0);
    }

    /**
     * To extract id
     *
     * @param value
     * @return
     */
    static String cleanValue(String value) {
        Pattern pattern = Pattern.compile(".*?(\\d+)&*.*?");
        Matcher matcher = pattern.matcher(value);
        while (matcher.find()) {
            return matcher.group(1);
        }

        return value;
    }

    /**
     * Remove prefix
     *
     * @param value
     * @return
     */
    static String cleanPhotoPrefix(String value) {
        Pattern pattern = Pattern.compile(".*?\\.(\\d+)&*.*?");
        if (value.contains("videos")) {
            pattern = Pattern.compile(".*?\\/(\\d+)\\/*.*?");
        }
        Matcher matcher = pattern.matcher(value);
        while (matcher.find()) {
            return matcher.group(1);
        }

        return value;
    }
}
