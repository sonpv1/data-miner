package net.crash.scrapper;

import net.crash.domain.Feed;
import net.crash.domain.GraphFeed;
import net.crash.domain.SearchResponse;
import net.crash.service.CommentService;
import net.crash.service.FeedService;
import net.crash.service.ReactionService;
import net.crash.utils.UrlBuilder;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static net.crash.scrapper.Search.Type.*;

@Component
public class Search {
    private final static Logger            LOGGER     = LoggerFactory.getLogger(Search.class);
    private static final Map<Type, String> URL_MAP    = new HashMap<>();
    private static final Map<Type, String> XPATH_MAP  = new HashMap<>();
    private              String            keyword    = "";
    private              Type              searchType = STR;

    @Value("${dataminer.facebookUrl}")
    private String facebookBasedUrl;

    @Autowired
    private FeedScrapper         feedScrapper;
    @Autowired
    private Facebook             facebook;
    @Autowired
    private SearchResponseParser responseParser;
    @Autowired
    private FeedService          feedService;
    @Autowired
    private ReactionService      reactionService;
    @Autowired
    private CommentService       commentService;

    public Search keyword(String keyword) {
        this.keyword = UrlBuilder.encode(keyword);
        return this;
    }

    public Search type(Type searchType) {
        this.searchType = searchType;
        return this;
    }

    public List<Feed> execute() {
        facebook.loadUrl(facebookBasedUrl);
        LOGGER.debug("SEARCH WITH KEYWORD: " + keyword);
        facebook.maxSize();
        ArrayList<Feed> dataList = new ArrayList<>();
        try {
            String url = URL_MAP.get(searchType).replace("{KEYWORD}", URLEncoder.encode(keyword, "UTF-8"));
            facebook.navigateTo(url);
        } catch (UnsupportedEncodingException e) {
            LOGGER.error("SEARCH EXCEPTION: ", e);
        }
        while (true) {
            facebook.executeScript("window.scrollBy(0,300)", "");
            if (facebook.getDriver().findElements(By.id("browse_end_of_results_footer")).size() > 0) {
                break;
            }
            facebook.wait(2000, TimeUnit.MILLISECONDS);
        }

        List<WebElement> links = facebook.getDriver().findElements(By.xpath(XPATH_MAP.get(searchType)));
        links.forEach(elem -> {
            try {
                SearchResponse searchResponse = responseParser.parse(elem);
                if (null == searchResponse || StringUtils.isEmpty(searchResponse.getFbId())) {
                    return;
                }
//                facebook.navigateTo(post.getPostUrl());
                //--- CALL API ---//
                if (StringUtils.isNotEmpty(searchResponse.getPageId()) || StringUtils.isNotEmpty(searchResponse.getGroupId())) {
                    facebook.wait(RandomUtils.nextInt(3, 11));
                    boolean isGroupPost = StringUtils.isNotEmpty(searchResponse.getGroupId());
                    GraphFeed data = feedScrapper.scrapeFeed(searchResponse.getFbId(), isGroupPost);
                    if (null != data) {
                        Map<String, Object> properties = data.getAdditionalProperties();
                        Feed feed = new Feed();
                        feed.setLastUpdate(System.currentTimeMillis());
                        feed.setFbid(data.getId());
                        feed.setUrl(searchResponse.getPostUrl());
                        feed.setFbpostType(searchResponse.getType());
                        feed.setFbpage(StringUtils.isEmpty(searchResponse.getPageId()) ? searchResponse.getGroupId() : searchResponse.getPageId());
                        feed.setDescription(isGroupPost ? properties.get("message").toString() : properties.get("name").toString());
                        feed.setFbpostDate(properties.get("created_time").toString());
                        feed.setImages(data.getImages());
                        if (null != data.getReactions() && null != data.getReactions().getSummary()) {
                            feed.setLikes(data.getReactions().getSummary().getTotalCount().longValue());
                            reactionService.save(data.getReactions().getData());
                        }
                        if (null != data.getComments() && null != data.getComments().getSummary()) {
                            feed.setComments(data.getComments().getSummary().getTotalCount().longValue());
                            commentService.save(data.getComments().getData());
                        }

                        feedService.save(feed);
                        dataList.add(feed);
                    }
                }

            } catch (Exception e) {
                LOGGER.error("SEARCH EXCEPTION: ", e);
            }
        });

        return dataList;
    }

    public enum Type {
        TOP, NEWEST, PERSON, PICTURE, VIDEO, PAGE, PLACE, GROUP, APP, EVENT, STR
    }

    @PostConstruct
    public void init() {
        URL_MAP.put(TOP,        facebookBasedUrl + "search/top/?init=quick&q=");
        URL_MAP.put(NEWEST,     facebookBasedUrl + "search/latest/?q=");
        URL_MAP.put(PERSON,     facebookBasedUrl + "search/people/?q=");
        URL_MAP.put(PICTURE,    facebookBasedUrl + "search/photos/?q=");
        URL_MAP.put(VIDEO,      facebookBasedUrl + "search/videos/?q=");
        URL_MAP.put(PAGE,       facebookBasedUrl + "search/pages/?q=");
        URL_MAP.put(PLACE,      facebookBasedUrl + "search/places/?q=");
        URL_MAP.put(GROUP,      facebookBasedUrl + "search/groups/?q=");
        URL_MAP.put(APP,        facebookBasedUrl + "search/apps/?q=");
        URL_MAP.put(EVENT,      facebookBasedUrl + "search/events/?q=");
        URL_MAP.put(STR,        facebookBasedUrl + "/search/str/{KEYWORD}/stories-keyword/intersect/stories-live");

        XPATH_MAP.put(TOP,      "//a[contains(@href,'ref=SEARCH&fref=nf')]");
        XPATH_MAP.put(NEWEST,   "//div[@class='_6a _5u5j _6b']//a");
        XPATH_MAP.put(PERSON,   "//div[@class='_gll']//a");
        XPATH_MAP.put(PICTURE,  "");
        XPATH_MAP.put(VIDEO,    "");
        XPATH_MAP.put(PAGE,     "");
        XPATH_MAP.put(PLACE,    "");
        XPATH_MAP.put(GROUP,    "");
        XPATH_MAP.put(APP,      "");
        XPATH_MAP.put(EVENT,    "");
        XPATH_MAP.put(STR,      "//*[@data-insertion-position!=\"\"]");
    }
}

