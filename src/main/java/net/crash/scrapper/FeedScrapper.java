package net.crash.scrapper;

import net.crash.domain.Comments;
import net.crash.domain.GraphFeed;
import net.crash.domain.Reactions;
import net.crash.utils.HttpRequestUtils;
import net.crash.utils.JSONUtils;
import net.crash.utils.UrlBuilder;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by Pi on 7/20/2017.
 */
@Component
public class FeedScrapper {

    private final static Logger LOGGER    = LoggerFactory.getLogger(FeedScrapper.class);
    private final static String FEED_PATH = "%s?access_token=%s&format=json&method=get&pretty=0&suppress_http_code=1&fields=images,comments.limit(25).summary(true),reactions.limit(25).summary(true),created_time";

    @Value("${dataminer.token}")
    private String facebookToken;
    @Value("${dataminer.graphBaseUrl}")
    private String graphBaseUrl;


    /**
     * To scrape feed information
     *
     * @param id
     * @return
     */
    public GraphFeed scrapeFeed(String id, boolean isGroupPost) {
        LOGGER.debug("Call Graph API to get feed");
        String path = FEED_PATH;
        if (isGroupPost) {
            path = path + "message,message_tags";
        } else {
            path = path + "name,name_tags";
        }
        path = String.format(path, id, UrlBuilder.encode(facebookToken));
        String baseUrl  = graphBaseUrl + path;
        String response = HttpRequestUtils.sendGet(baseUrl);
        if (StringUtils.isNotEmpty(response)) {
            GraphFeed feed = JSONUtils.fromJSON(response, GraphFeed.class);
            if (null != feed.getComments() && null != feed.getComments().getPaging() && StringUtils.isNotEmpty(feed.getComments().getPaging().getNext())) {
                String   next     = feed.getComments().getPaging().getNext();
                Comments comments = scrapeComments(next);
                if (null != comments) {
                    feed.getComments().getData().addAll(comments.getData());
                }
            }
            if (null != feed.getLikes() && null != feed.getLikes().getPaging() && StringUtils.isNotEmpty(feed.getLikes().getPaging().getNext())) {
                String    next      = feed.getLikes().getPaging().getNext();
                Reactions reactions = scrapeReactions(next);
                if (null != reactions) {
                    feed.getReactions().getData().addAll(reactions.getData());
                }
            }
            LOGGER.info("FEED: ==> " + JSONUtils.toString(feed));
        }

        return null;
    }

    /**
     * To scrape comments
     *
     * @param url
     * @return
     */
    public Comments scrapeComments(String url) {
        LOGGER.debug("Call Graph API to get next comments");
        String response = HttpRequestUtils.sendGet(url);
        if (StringUtils.isNotEmpty(response)) {
            Comments comments = JSONUtils.fromJSON(response, Comments.class);
            if (null == comments.getPaging() || StringUtils.isEmpty(comments.getPaging().getNext())) {
                return comments;
            }
            if (StringUtils.isNotEmpty(comments.getPaging().getNext())) {
                String nextUrl = UrlBuilder.decodeUrl(comments.getPaging().getNext());
                nextUrl = nextUrl.replace("|", UrlBuilder.encode("|"));
                LOGGER.info("NEXT: " + nextUrl);
                Comments next = scrapeComments(nextUrl);
                if (null != next) {
                    comments.getData().addAll(next.getData());
                }
            }

            return comments;
        }

        return null;
    }

    /**
     * To scrape reactions
     *
     * @param url
     * @return
     */
    public Reactions scrapeReactions(String url) {
        LOGGER.debug("Call Graph API to get next reaction");
        String response = HttpRequestUtils.sendGet(url);
        if (StringUtils.isNotEmpty(response)) {
            Reactions reactions = JSONUtils.fromJSON(response, Reactions.class);
            if (null == reactions.getPaging() || StringUtils.isEmpty(reactions.getPaging().getNext())) {
                return reactions;
            }
            if (StringUtils.isNotEmpty(reactions.getPaging().getNext())) {
                String nextUrl = UrlBuilder.decodeUrl(reactions.getPaging().getNext());
                nextUrl = nextUrl.replace("|", UrlBuilder.encode("|"));
                LOGGER.info("NEXT: " + nextUrl);
                Reactions next = scrapeReactions(nextUrl);
                if (null != next) {
                    reactions.getData().addAll(next.getData());
                }
            }

            return reactions;
        }

        return null;
    }


}
