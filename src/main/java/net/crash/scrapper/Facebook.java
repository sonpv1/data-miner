package net.crash.scrapper;

import net.crash.utils.IOUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Component
public class Facebook implements Serializable {

    private final static Logger    LOGGER = LoggerFactory.getLogger(Facebook.class);

    @Value("${dataminer.facebookUrl}")
    private String facebookBasedUrl;
    @Value("${dataminer.cookiePath}")
    private String cookiePath;
    @Value("${dataminer.reuseCookie}")
    private String reuseCookie;
    @Value("${dataminer.username}")
    private String username;
    @Value("${dataminer.password}")
    private String password;
    @Value("${dataminer.driverPath}")
    private String driverPath;

    private boolean loggedIn = false;
    private static WebDriver driver;

    @PostConstruct
    public void init() {
        System.setProperty("webdriver.chrome.driver", driverPath);
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("profile.default_content_setting_values.notifications", 2);

        ChromeOptions options = new ChromeOptions();
        options.addArguments("test-type");
        options.addArguments("--disable-extensions");
        options.setExperimentalOption("prefs", prefs);

        DesiredCapabilities dc = DesiredCapabilities.chrome();
        dc.setCapability(ChromeOptions.CAPABILITY, options);
        driver = new ChromeDriver(dc);
    }

    /**
     * To load url
     *
     * @param url
     */
    public void loadUrl(String url) {
        driver.get(url);
        if (!loggedIn) {
            if ("true".equalsIgnoreCase(reuseCookie)) {
                loadCookies();
            } else {
                login();
            }
        }
    }

    /**
     * To navigate to other url
     *
     * @param url
     */
    public void navigateTo(String url) {
        if (!loggedIn) {
            if ("true".equalsIgnoreCase(reuseCookie)) {
                loadCookies();
            } else {
                login();
            }
        }
        driver.navigate().to(url);
    }

    /**
     * To sleep in amount of timeunit
     *
     * @param n
     * @param timeUnit
     */
    public void wait(int n, TimeUnit timeUnit) {
        driver.manage().timeouts().implicitlyWait(n, timeUnit);
    }

    /**
     * To execute javascript
     *
     * @param script
     * @param args
     */
    public void executeScript(String script, Object... args) {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript(script, args);
    }

    /**
     * To login to facebook
     *
     */
    public void login() {
        maxSize();
        driver.get(facebookBasedUrl);

        WebElement emailInput = driver.findElement(By.name("email"));
        emailInput.sendKeys(username);

        WebElement passwordInput = driver.findElement(By.name("pass"));
        passwordInput.sendKeys(password);

        emailInput.submit();

        loggedIn = true;
        saveCookie();
    }

    /**
     * To set max size of browser
     */
    public void maxSize() {
        driver.manage().window().maximize();
    }

    /**
     * To sleep for a second
     *
     * @param second
     */
    public void wait(int second) {
        driver.manage().timeouts().implicitlyWait(second, TimeUnit.SECONDS);
    }

    /**
     * To save cookies to file
     */
    public void saveCookie() {
        LOGGER.debug("SAVE COOKIES TO FILE");
        Set<Cookie> cookies = driver.manage().getCookies();
        IOUtils.save(cookies, cookiePath);
    }

    /**
     * To load cookies from file
     */
    public void loadCookies() {
        LOGGER.debug("LOAD COOKIES FROM FILE");
        Set<Cookie> cookies = (Set<Cookie>) IOUtils.load(cookiePath);
        cookies.forEach(driver.manage()::addCookie);
        cookies.forEach(System.out::println);
        loggedIn = true;
    }

    public WebDriver getDriver() {
        return driver;
    }
}
