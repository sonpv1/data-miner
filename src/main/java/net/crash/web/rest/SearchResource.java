package net.crash.web.rest;

import com.codahale.metrics.annotation.Timed;
import net.crash.domain.Feed;
import net.crash.domain.Keyword;
import net.crash.scrapper.Search;
import net.crash.service.KeywordService;
import net.crash.utils.UrlBuilder;
import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Pi on 7/21/2017.
 */
@RestController
@RequestMapping("/api")
public class SearchResource {

    private final Logger log = LoggerFactory.getLogger(SearchResource.class);

    @Autowired
    private Search search;
    @Autowired
    private KeywordService keywordService;

    @GetMapping("/search")
    @Timed
    public ResponseEntity<List<Feed>> search(@RequestParam(value = "keyword") String keyword) {
        log.debug("REST request to search with keyword : {}", keyword);
        if (StringUtils.isEmpty(keyword)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<Feed> feeds = search.keyword(keyword).execute();
        return new ResponseEntity<>(feeds, HttpStatus.OK);
    }

    @GetMapping("/crawl")
    @Timed
    public ResponseEntity<Void> crawl() {
        log.debug("REST request to crawl data by all keywords");
        Page<Keyword> keywordPage = keywordService.findAll(new PageRequest(0, Integer.MAX_VALUE));
        if (null != keywordPage.getContent()) {
            keywordPage.getContent().forEach(keyword -> {
                if (StringUtils.isNotEmpty(keyword.getKeyword())) {
                    search.keyword(keyword.getKeyword()).execute();
                }
            });
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/crawl/{id}")
    @Timed
    public ResponseEntity<List<Feed>> crawl(@PathVariable String id) {
        log.debug("REST request to crawl data by keyword : {}", id);
        Keyword keyword = keywordService.findOne(id);
        if (StringUtils.isNotEmpty(keyword.getKeyword())) {
            List<Feed> feeds = search.keyword(keyword.getKeyword()).execute();
            return new ResponseEntity<>(feeds, HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
