/**
 * View Models used by Spring MVC REST controllers.
 */
package net.crash.web.rest.vm;
