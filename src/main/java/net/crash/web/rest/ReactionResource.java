package net.crash.web.rest;

import com.codahale.metrics.annotation.Timed;
import net.crash.domain.Reaction;
import net.crash.service.ReactionService;
import net.crash.web.rest.util.HeaderUtil;
import net.crash.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Reaction.
 */
@RestController
@RequestMapping("/api")
public class ReactionResource {

    private final Logger log = LoggerFactory.getLogger(ReactionResource.class);

    private static final String ENTITY_NAME = "reaction";

    private final ReactionService reactionService;

    public ReactionResource(ReactionService reactionService) {
        this.reactionService = reactionService;
    }

    /**
     * POST  /reactions : Create a new reaction.
     *
     * @param reaction the reaction to create
     * @return the ResponseEntity with status 201 (Created) and with body the new reaction, or with status 400 (Bad Request) if the reaction has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/reactions")
    @Timed
    public ResponseEntity<Reaction> createReaction(@RequestBody Reaction reaction) throws URISyntaxException {
        log.debug("REST request to save Reaction : {}", reaction);
        if (reaction.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new reaction cannot already have an ID")).body(null);
        }
        Reaction result = reactionService.save(reaction);
        return ResponseEntity.created(new URI("/api/reactions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /reactions : Updates an existing reaction.
     *
     * @param reaction the reaction to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated reaction,
     * or with status 400 (Bad Request) if the reaction is not valid,
     * or with status 500 (Internal Server Error) if the reaction couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/reactions")
    @Timed
    public ResponseEntity<Reaction> updateReaction(@RequestBody Reaction reaction) throws URISyntaxException {
        log.debug("REST request to update Reaction : {}", reaction);
        if (reaction.getId() == null) {
            return createReaction(reaction);
        }
        Reaction result = reactionService.save(reaction);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, reaction.getId().toString()))
            .body(result);
    }

    /**
     * GET  /reactions : get all the reactions.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of reactions in body
     */
    @GetMapping("/reactions")
    @Timed
    public ResponseEntity<List<Reaction>> getAllReactions(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Reactions");
        Page<Reaction> page = reactionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/reactions");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /reactions/:id : get the "id" reaction.
     *
     * @param id the id of the reaction to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the reaction, or with status 404 (Not Found)
     */
    @GetMapping("/reactions/{id}")
    @Timed
    public ResponseEntity<Reaction> getReaction(@PathVariable String id) {
        log.debug("REST request to get Reaction : {}", id);
        Reaction reaction = reactionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(reaction));
    }

    /**
     * DELETE  /reactions/:id : delete the "id" reaction.
     *
     * @param id the id of the reaction to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/reactions/{id}")
    @Timed
    public ResponseEntity<Void> deleteReaction(@PathVariable String id) {
        log.debug("REST request to delete Reaction : {}", id);
        reactionService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}
