package net.crash.domain;


import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "data",
        "paging",
        "summary"
})
public class Likes {

    @JsonProperty("data")
    private List<Like> data = null;
    @JsonProperty("paging")
    private Paging      paging;
    @JsonProperty("summary")
    private LikeSummary summary;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("data")
    public List<Like> getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(List<Like> data) {
        this.data = data;
    }

    @JsonProperty("paging")
    public Paging getPaging() {
        return paging;
    }

    @JsonProperty("paging")
    public void setPaging(Paging paging) {
        this.paging = paging;
    }

    @JsonProperty("summary")
    public LikeSummary getSummary() {
        return summary;
    }

    @JsonProperty("summary")
    public void setSummary(LikeSummary summary) {
        this.summary = summary;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
