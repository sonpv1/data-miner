package net.crash.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Comment.
 */
@Document(collection = "comment")
public class Comment implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private String id;

    @Field("fbid")
    private String fbid;

    @Field("message")
    private String message;

    @Field("created_time")
    private String createdTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFbid() {
        return fbid;
    }

    public Comment fbid(String fbid) {
        this.fbid = fbid;
        return this;
    }

    public void setFbid(String fbid) {
        this.fbid = fbid;
    }

    public String getMessage() {
        return message;
    }

    public Comment message(String message) {
        this.message = message;
        return this;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public Comment createdTime(String createdTime) {
        this.createdTime = createdTime;
        return this;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Comment comment = (Comment) o;
        if (comment.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), comment.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Comment{" +
            "id=" + getId() +
            ", fbid='" + getFbid() + "'" +
            ", message='" + getMessage() + "'" +
            ", createdTime='" + getCreatedTime() + "'" +
            "}";
    }
}
