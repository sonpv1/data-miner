package net.crash.domain;

/**
 * Created by Pi on 7/21/2017.
 */
public class SearchResponse {
    private String fbId;
    private String userId;
    private String user;
    private String userUrl;
    private String postUrl;
    private String page;
    private String pageId;
    private String group;
    private String groupId;
    private String type;

    @Override
    public String toString() {
        return "Post{" +
            "fbId='" + fbId + '\'' +
            ", userId='" + userId + '\'' +
            ", userUrl='" + userUrl + '\'' +
            ", postUrl='" + postUrl + '\'' +
            ", page='" + page + '\'' +
            ", pageId='" + pageId + '\'' +
            ", group='" + group + '\'' +
            ", groupId='" + groupId + '\'' +
            ", type='" + type + '\'' +
            '}';
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getFbId() {
        return fbId;
    }

    public void setFbId(String fbId) {
        this.fbId = fbId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserUrl() {
        return userUrl;
    }

    public void setUserUrl(String userUrl) {
        this.userUrl = userUrl;
    }

    public String getPostUrl() {
        return postUrl;
    }

    public void setPostUrl(String postUrl) {
        this.postUrl = postUrl;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
