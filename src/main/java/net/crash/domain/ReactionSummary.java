package net.crash.domain;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "total_count",
        "viewer_reaction"
})
public class ReactionSummary {

    @JsonProperty("total_count")
    private Integer totalCount;
    @JsonProperty("viewer_reaction")
    private String viewerReaction;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("total_count")
    public Integer getTotalCount() {
        return totalCount;
    }

    @JsonProperty("total_count")
    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    @JsonProperty("viewer_reaction")
    public String getViewerReaction() {
        return viewerReaction;
    }

    @JsonProperty("viewer_reaction")
    public void setViewerReaction(String viewerReaction) {
        this.viewerReaction = viewerReaction;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
