package net.crash.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * A Feed.
 */
@Document(collection = "feed")
public class Feed implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private String id;

    @Field("fbid")
    private String fbid;

    @Field("title")
    private String title;

    @Field("url")
    private String url;

    @Field("image_url")
    private String imageUrl;

    @Field("fbpost_type")
    private String fbpostType;

    @Field("likes")
    private Long likes;

    @Field("comments")
    private Long comments;

    @Field("shares")
    private String shares;

    @Field("fbpost_date")
    private String fbpostDate;

    @Field("description")
    private String description;

    @Field("link")
    private String link;

    @Field("fbpage")
    private String fbpage;

    @Field("last_update")
    private Long lastUpdate;

    @JsonProperty("images")
    private List<Image> images = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFbid() {
        return fbid;
    }

    public Feed fbid(String fbid) {
        this.fbid = fbid;
        return this;
    }

    public void setFbid(String fbid) {
        this.fbid = fbid;
    }

    public String getTitle() {
        return title;
    }

    public Feed title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public Feed url(String url) {
        this.url = url;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Feed imageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getFbpostType() {
        return fbpostType;
    }

    public Feed fbpostType(String fbpostType) {
        this.fbpostType = fbpostType;
        return this;
    }

    public void setFbpostType(String fbpostType) {
        this.fbpostType = fbpostType;
    }

    public Long getLikes() {
        return likes;
    }

    public Feed likes(Long likes) {
        this.likes = likes;
        return this;
    }

    public void setLikes(Long likes) {
        this.likes = likes;
    }

    public Long getComments() {
        return comments;
    }

    public Feed comments(Long comments) {
        this.comments = comments;
        return this;
    }

    public void setComments(Long comments) {
        this.comments = comments;
    }

    public String getShares() {
        return shares;
    }

    public Feed shares(String shares) {
        this.shares = shares;
        return this;
    }

    public void setShares(String shares) {
        this.shares = shares;
    }

    public String getFbpostDate() {
        return fbpostDate;
    }

    public Feed fbpostDate(String fbpostDate) {
        this.fbpostDate = fbpostDate;
        return this;
    }

    public void setFbpostDate(String fbpostDate) {
        this.fbpostDate = fbpostDate;
    }

    public String getDescription() {
        return description;
    }

    public Feed description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public Feed link(String link) {
        this.link = link;
        return this;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getFbpage() {
        return fbpage;
    }

    public Feed fbpage(String fbpage) {
        this.fbpage = fbpage;
        return this;
    }

    public void setFbpage(String fbpage) {
        this.fbpage = fbpage;
    }

    public Long getLastUpdate() {
        return lastUpdate;
    }

    public Feed lastUpdate(Long lastUpdate) {
        this.lastUpdate = lastUpdate;
        return this;
    }

    public void setLastUpdate(Long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @JsonProperty("images")
    public List<Image> getImages() {
        return images;
    }

    @JsonProperty("images")
    public void setImages(List<Image> images) {
        this.images = images;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Feed feed = (Feed) o;
        if (feed.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), feed.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Feed{" +
            "id=" + getId() +
            ", fbid='" + getFbid() + "'" +
            ", title='" + getTitle() + "'" +
            ", url='" + getUrl() + "'" +
            ", imageUrl='" + getImageUrl() + "'" +
            ", fbpostType='" + getFbpostType() + "'" +
            ", likes='" + getLikes() + "'" +
            ", comments='" + getComments() + "'" +
            ", shares='" + getShares() + "'" +
            ", fbpostDate='" + getFbpostDate() + "'" +
            ", description='" + getDescription() + "'" +
            ", link='" + getLink() + "'" +
            ", fbpage='" + getFbpage() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            "}";
    }
}
