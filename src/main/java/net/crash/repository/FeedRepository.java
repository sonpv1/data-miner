package net.crash.repository;

import net.crash.domain.Feed;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Feed entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FeedRepository extends MongoRepository<Feed,String> {
    
}
