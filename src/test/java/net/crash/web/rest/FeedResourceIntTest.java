package net.crash.web.rest;

import net.crash.DataminerApp;

import net.crash.domain.Feed;
import net.crash.repository.FeedRepository;
import net.crash.service.FeedService;
import net.crash.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the FeedResource REST controller.
 *
 * @see FeedResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DataminerApp.class)
public class FeedResourceIntTest {

    private static final String DEFAULT_FBID = "AAAAAAAAAA";
    private static final String UPDATED_FBID = "BBBBBBBBBB";

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_URL = "AAAAAAAAAA";
    private static final String UPDATED_URL = "BBBBBBBBBB";

    private static final String DEFAULT_IMAGE_URL = "AAAAAAAAAA";
    private static final String UPDATED_IMAGE_URL = "BBBBBBBBBB";

    private static final String DEFAULT_FBPOST_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_FBPOST_TYPE = "BBBBBBBBBB";

    private static final Long DEFAULT_LIKES = 1L;
    private static final Long UPDATED_LIKES = 2L;

    private static final Long DEFAULT_COMMENTS = 1L;
    private static final Long UPDATED_COMMENTS = 2L;

    private static final String DEFAULT_SHARES = "AAAAAAAAAA";
    private static final String UPDATED_SHARES = "BBBBBBBBBB";

    private static final String DEFAULT_FBPOST_DATE = "AAAAAAAAAA";
    private static final String UPDATED_FBPOST_DATE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_LINK = "AAAAAAAAAA";
    private static final String UPDATED_LINK = "BBBBBBBBBB";

    private static final String DEFAULT_FBPAGE = "AAAAAAAAAA";
    private static final String UPDATED_FBPAGE = "BBBBBBBBBB";

    private static final Long DEFAULT_LAST_UPDATE = 1L;
    private static final Long UPDATED_LAST_UPDATE = 2L;

    @Autowired
    private FeedRepository feedRepository;

    @Autowired
    private FeedService feedService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restFeedMockMvc;

    private Feed feed;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        FeedResource feedResource = new FeedResource(feedService);
        this.restFeedMockMvc = MockMvcBuilders.standaloneSetup(feedResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Feed createEntity() {
        Feed feed = new Feed()
            .fbid(DEFAULT_FBID)
            .title(DEFAULT_TITLE)
            .url(DEFAULT_URL)
            .imageUrl(DEFAULT_IMAGE_URL)
            .fbpostType(DEFAULT_FBPOST_TYPE)
            .likes(DEFAULT_LIKES)
            .comments(DEFAULT_COMMENTS)
            .shares(DEFAULT_SHARES)
            .fbpostDate(DEFAULT_FBPOST_DATE)
            .description(DEFAULT_DESCRIPTION)
            .link(DEFAULT_LINK)
            .fbpage(DEFAULT_FBPAGE)
            .lastUpdate(DEFAULT_LAST_UPDATE);
        return feed;
    }

    @Before
    public void initTest() {
        feedRepository.deleteAll();
        feed = createEntity();
    }

    @Test
    public void createFeed() throws Exception {
        int databaseSizeBeforeCreate = feedRepository.findAll().size();

        // Create the Feed
        restFeedMockMvc.perform(post("/api/feeds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(feed)))
            .andExpect(status().isCreated());

        // Validate the Feed in the database
        List<Feed> feedList = feedRepository.findAll();
        assertThat(feedList).hasSize(databaseSizeBeforeCreate + 1);
        Feed testFeed = feedList.get(feedList.size() - 1);
        assertThat(testFeed.getFbid()).isEqualTo(DEFAULT_FBID);
        assertThat(testFeed.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testFeed.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testFeed.getImageUrl()).isEqualTo(DEFAULT_IMAGE_URL);
        assertThat(testFeed.getFbpostType()).isEqualTo(DEFAULT_FBPOST_TYPE);
        assertThat(testFeed.getLikes()).isEqualTo(DEFAULT_LIKES);
        assertThat(testFeed.getComments()).isEqualTo(DEFAULT_COMMENTS);
        assertThat(testFeed.getShares()).isEqualTo(DEFAULT_SHARES);
        assertThat(testFeed.getFbpostDate()).isEqualTo(DEFAULT_FBPOST_DATE);
        assertThat(testFeed.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testFeed.getLink()).isEqualTo(DEFAULT_LINK);
        assertThat(testFeed.getFbpage()).isEqualTo(DEFAULT_FBPAGE);
        assertThat(testFeed.getLastUpdate()).isEqualTo(DEFAULT_LAST_UPDATE);
    }

    @Test
    public void createFeedWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = feedRepository.findAll().size();

        // Create the Feed with an existing ID
        feed.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restFeedMockMvc.perform(post("/api/feeds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(feed)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Feed> feedList = feedRepository.findAll();
        assertThat(feedList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllFeeds() throws Exception {
        // Initialize the database
        feedRepository.save(feed);

        // Get all the feedList
        restFeedMockMvc.perform(get("/api/feeds?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(feed.getId())))
            .andExpect(jsonPath("$.[*].fbid").value(hasItem(DEFAULT_FBID.toString())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
            .andExpect(jsonPath("$.[*].imageUrl").value(hasItem(DEFAULT_IMAGE_URL.toString())))
            .andExpect(jsonPath("$.[*].fbpostType").value(hasItem(DEFAULT_FBPOST_TYPE.toString())))
            .andExpect(jsonPath("$.[*].likes").value(hasItem(DEFAULT_LIKES.intValue())))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS.intValue())))
            .andExpect(jsonPath("$.[*].shares").value(hasItem(DEFAULT_SHARES.toString())))
            .andExpect(jsonPath("$.[*].fbpostDate").value(hasItem(DEFAULT_FBPOST_DATE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].link").value(hasItem(DEFAULT_LINK.toString())))
            .andExpect(jsonPath("$.[*].fbpage").value(hasItem(DEFAULT_FBPAGE.toString())))
            .andExpect(jsonPath("$.[*].lastUpdate").value(hasItem(DEFAULT_LAST_UPDATE.intValue())));
    }

    @Test
    public void getFeed() throws Exception {
        // Initialize the database
        feedRepository.save(feed);

        // Get the feed
        restFeedMockMvc.perform(get("/api/feeds/{id}", feed.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(feed.getId()))
            .andExpect(jsonPath("$.fbid").value(DEFAULT_FBID.toString()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()))
            .andExpect(jsonPath("$.imageUrl").value(DEFAULT_IMAGE_URL.toString()))
            .andExpect(jsonPath("$.fbpostType").value(DEFAULT_FBPOST_TYPE.toString()))
            .andExpect(jsonPath("$.likes").value(DEFAULT_LIKES.intValue()))
            .andExpect(jsonPath("$.comments").value(DEFAULT_COMMENTS.intValue()))
            .andExpect(jsonPath("$.shares").value(DEFAULT_SHARES.toString()))
            .andExpect(jsonPath("$.fbpostDate").value(DEFAULT_FBPOST_DATE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.link").value(DEFAULT_LINK.toString()))
            .andExpect(jsonPath("$.fbpage").value(DEFAULT_FBPAGE.toString()))
            .andExpect(jsonPath("$.lastUpdate").value(DEFAULT_LAST_UPDATE.intValue()));
    }

    @Test
    public void getNonExistingFeed() throws Exception {
        // Get the feed
        restFeedMockMvc.perform(get("/api/feeds/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateFeed() throws Exception {
        // Initialize the database
        feedService.save(feed);

        int databaseSizeBeforeUpdate = feedRepository.findAll().size();

        // Update the feed
        Feed updatedFeed = feedRepository.findOne(feed.getId());
        updatedFeed
            .fbid(UPDATED_FBID)
            .title(UPDATED_TITLE)
            .url(UPDATED_URL)
            .imageUrl(UPDATED_IMAGE_URL)
            .fbpostType(UPDATED_FBPOST_TYPE)
            .likes(UPDATED_LIKES)
            .comments(UPDATED_COMMENTS)
            .shares(UPDATED_SHARES)
            .fbpostDate(UPDATED_FBPOST_DATE)
            .description(UPDATED_DESCRIPTION)
            .link(UPDATED_LINK)
            .fbpage(UPDATED_FBPAGE)
            .lastUpdate(UPDATED_LAST_UPDATE);

        restFeedMockMvc.perform(put("/api/feeds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedFeed)))
            .andExpect(status().isOk());

        // Validate the Feed in the database
        List<Feed> feedList = feedRepository.findAll();
        assertThat(feedList).hasSize(databaseSizeBeforeUpdate);
        Feed testFeed = feedList.get(feedList.size() - 1);
        assertThat(testFeed.getFbid()).isEqualTo(UPDATED_FBID);
        assertThat(testFeed.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testFeed.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testFeed.getImageUrl()).isEqualTo(UPDATED_IMAGE_URL);
        assertThat(testFeed.getFbpostType()).isEqualTo(UPDATED_FBPOST_TYPE);
        assertThat(testFeed.getLikes()).isEqualTo(UPDATED_LIKES);
        assertThat(testFeed.getComments()).isEqualTo(UPDATED_COMMENTS);
        assertThat(testFeed.getShares()).isEqualTo(UPDATED_SHARES);
        assertThat(testFeed.getFbpostDate()).isEqualTo(UPDATED_FBPOST_DATE);
        assertThat(testFeed.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testFeed.getLink()).isEqualTo(UPDATED_LINK);
        assertThat(testFeed.getFbpage()).isEqualTo(UPDATED_FBPAGE);
        assertThat(testFeed.getLastUpdate()).isEqualTo(UPDATED_LAST_UPDATE);
    }

    @Test
    public void updateNonExistingFeed() throws Exception {
        int databaseSizeBeforeUpdate = feedRepository.findAll().size();

        // Create the Feed

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restFeedMockMvc.perform(put("/api/feeds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(feed)))
            .andExpect(status().isCreated());

        // Validate the Feed in the database
        List<Feed> feedList = feedRepository.findAll();
        assertThat(feedList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteFeed() throws Exception {
        // Initialize the database
        feedService.save(feed);

        int databaseSizeBeforeDelete = feedRepository.findAll().size();

        // Get the feed
        restFeedMockMvc.perform(delete("/api/feeds/{id}", feed.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Feed> feedList = feedRepository.findAll();
        assertThat(feedList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Feed.class);
        Feed feed1 = new Feed();
        feed1.setId("id1");
        Feed feed2 = new Feed();
        feed2.setId(feed1.getId());
        assertThat(feed1).isEqualTo(feed2);
        feed2.setId("id2");
        assertThat(feed1).isNotEqualTo(feed2);
        feed1.setId(null);
        assertThat(feed1).isNotEqualTo(feed2);
    }
}
