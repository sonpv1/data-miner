/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { DataminerTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { KeywordDataMinerDetailComponent } from '../../../../../../main/webapp/app/entities/keyword/keyword-data-miner-detail.component';
import { KeywordDataMinerService } from '../../../../../../main/webapp/app/entities/keyword/keyword-data-miner.service';
import { KeywordDataMiner } from '../../../../../../main/webapp/app/entities/keyword/keyword-data-miner.model';

describe('Component Tests', () => {

    describe('KeywordDataMiner Management Detail Component', () => {
        let comp: KeywordDataMinerDetailComponent;
        let fixture: ComponentFixture<KeywordDataMinerDetailComponent>;
        let service: KeywordDataMinerService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [DataminerTestModule],
                declarations: [KeywordDataMinerDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    KeywordDataMinerService,
                    JhiEventManager
                ]
            }).overrideTemplate(KeywordDataMinerDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(KeywordDataMinerDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(KeywordDataMinerService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new KeywordDataMiner('aaa')));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.keyword).toEqual(jasmine.objectContaining({id: 'aaa'}));
            });
        });
    });

});
