/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { DataminerTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { FeedDataMinerDetailComponent } from '../../../../../../main/webapp/app/entities/feed/feed-data-miner-detail.component';
import { FeedDataMinerService } from '../../../../../../main/webapp/app/entities/feed/feed-data-miner.service';
import { FeedDataMiner } from '../../../../../../main/webapp/app/entities/feed/feed-data-miner.model';

describe('Component Tests', () => {

    describe('FeedDataMiner Management Detail Component', () => {
        let comp: FeedDataMinerDetailComponent;
        let fixture: ComponentFixture<FeedDataMinerDetailComponent>;
        let service: FeedDataMinerService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [DataminerTestModule],
                declarations: [FeedDataMinerDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    FeedDataMinerService,
                    JhiEventManager
                ]
            }).overrideTemplate(FeedDataMinerDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(FeedDataMinerDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(FeedDataMinerService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new FeedDataMiner('aaa')));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.feed).toEqual(jasmine.objectContaining({id: 'aaa'}));
            });
        });
    });

});
