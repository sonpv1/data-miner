/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { DataminerTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { CommentDataMinerDetailComponent } from '../../../../../../main/webapp/app/entities/comment/comment-data-miner-detail.component';
import { CommentDataMinerService } from '../../../../../../main/webapp/app/entities/comment/comment-data-miner.service';
import { CommentDataMiner } from '../../../../../../main/webapp/app/entities/comment/comment-data-miner.model';

describe('Component Tests', () => {

    describe('CommentDataMiner Management Detail Component', () => {
        let comp: CommentDataMinerDetailComponent;
        let fixture: ComponentFixture<CommentDataMinerDetailComponent>;
        let service: CommentDataMinerService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [DataminerTestModule],
                declarations: [CommentDataMinerDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    CommentDataMinerService,
                    JhiEventManager
                ]
            }).overrideTemplate(CommentDataMinerDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CommentDataMinerDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CommentDataMinerService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new CommentDataMiner('aaa')));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.comment).toEqual(jasmine.objectContaining({id: 'aaa'}));
            });
        });
    });

});
