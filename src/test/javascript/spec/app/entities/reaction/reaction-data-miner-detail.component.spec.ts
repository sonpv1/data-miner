/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { DataminerTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { ReactionDataMinerDetailComponent } from '../../../../../../main/webapp/app/entities/reaction/reaction-data-miner-detail.component';
import { ReactionDataMinerService } from '../../../../../../main/webapp/app/entities/reaction/reaction-data-miner.service';
import { ReactionDataMiner } from '../../../../../../main/webapp/app/entities/reaction/reaction-data-miner.model';

describe('Component Tests', () => {

    describe('ReactionDataMiner Management Detail Component', () => {
        let comp: ReactionDataMinerDetailComponent;
        let fixture: ComponentFixture<ReactionDataMinerDetailComponent>;
        let service: ReactionDataMinerService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [DataminerTestModule],
                declarations: [ReactionDataMinerDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    ReactionDataMinerService,
                    JhiEventManager
                ]
            }).overrideTemplate(ReactionDataMinerDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ReactionDataMinerDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ReactionDataMinerService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new ReactionDataMiner('aaa')));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.reaction).toEqual(jasmine.objectContaining({id: 'aaa'}));
            });
        });
    });

});
